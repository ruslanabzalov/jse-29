package tsc.abzalov.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.domain.Domain;
import tsc.abzalov.tm.enumeration.CommandType;

import java.io.File;

import static tsc.abzalov.tm.enumeration.CommandType.USER_COMMAND;

@SuppressWarnings("unused")
public final class AutoLoadBackupCommand extends AbstractDomainCommand {

    public AutoLoadBackupCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "backup-load";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Backup load command.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return USER_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull val objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        @NotNull val file = new File(BACKUP_FILENAME);

        if (!file.exists()) return;

        @NotNull val domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
    }
}

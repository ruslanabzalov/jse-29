package tsc.abzalov.tm.component;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("FieldCanBeLocal")
public final class BackupComponent {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final String BACKUP_LOAD_COMMAND_NAME = "backup-load";

    @NotNull
    private final String BACKUP_SAVE_COMMAND_NAME = "backup-save";

    private final int INITIAL_DELAY = 1;

    private final int DELAY = 30;

    public BackupComponent(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void init() {
        loadBackup();
        scheduledBackupSave();
    }

    private void loadBackup() {
        executeBackupLoadCommand();
    }

    private void scheduledBackupSave() {
        executorService.scheduleWithFixedDelay(this::executeBackupSaveCommand, INITIAL_DELAY, DELAY, TimeUnit.SECONDS);
    }

    private void executeBackupLoadCommand() {
        executeCommand(BACKUP_LOAD_COMMAND_NAME);
    }

    private void executeBackupSaveCommand() {
        executeCommand(BACKUP_SAVE_COMMAND_NAME);
    }

    private void executeCommand(@NotNull final String commandName) {
        @NotNull val commandService = serviceLocator.getCommandService();
        @NotNull val command = commandService.getCommandByName(commandName);
        command.execute();
    }

}

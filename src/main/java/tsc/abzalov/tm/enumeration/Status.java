package tsc.abzalov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Status {

    TODO("TODO"),
    IN_PROGRESS("In Progress"),
    DONE("Done");

    @NotNull
    private final String statusName;

    Status(@NotNull final String statusName) {
        this.statusName = statusName;
    }

    @NotNull
    public String getStatusName() {
        return statusName;
    }

}

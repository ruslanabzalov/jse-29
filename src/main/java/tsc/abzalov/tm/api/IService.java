package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@SuppressWarnings("unused")
public interface IService<T> {

    long size();

    boolean isEmpty();

    void create(@Nullable T entity);

    void addAll(@Nullable List<T> entities);

    @NotNull
    List<T> findAll();

    @Nullable
    T findById(@Nullable String id);

    void clear();

    void removeById(@Nullable String id);

}

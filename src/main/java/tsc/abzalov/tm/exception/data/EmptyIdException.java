package tsc.abzalov.tm.exception.data;

import tsc.abzalov.tm.exception.AbstractException;

public final class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("ID is empty!");
    }

}
